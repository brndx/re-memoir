﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshEnable : MonoBehaviour
{
    public Renderer myRenderer;
    public MeshCollider myCollider;
    public GameObject whiteRoom;
    public bool passed;

    void Start()
    {
        //myCollider = GetComponent<MeshCollider>();
        myRenderer.enabled = false;
        myCollider.enabled = false;
        whiteRoom.SetActive(true);

    }
    

    void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            myRenderer.enabled = true;
            myCollider.enabled = true;
            whiteRoom.SetActive(false);
            gameObject.SetActive(false);

        }
        
       
    }


}
