﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSound : MonoBehaviour {
    private float lastTime = 0.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - lastTime > 20)
        {
            int n = Random.Range(0,1);
            if (n == 1)
                GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
            lastTime = Time.time;
        }
	}
}
