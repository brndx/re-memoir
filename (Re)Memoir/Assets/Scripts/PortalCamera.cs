﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    [SerializeField]private Transform playerCamera;

	void Update ()
    {
        transform.eulerAngles = new Vector3(-playerCamera.eulerAngles.x, playerCamera.eulerAngles.y, playerCamera.eulerAngles.z);
    }
}
