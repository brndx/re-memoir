﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;
using Transit_Door;
using DarkFigure;
namespace MainMenu {
    public class MainMenuScript : MonoBehaviour
    {
        [SerializeField] private Image fade;
        [SerializeField] private Text text;
        [SerializeField] private Material[] Skyboxes;
        [SerializeField] private FigureEmission darkFigure;
        private AudioSource music;
        private GameObject TV;
        private bool darkAmbientPlayed = false;
        private GameObject triggerCube;
        private AudioSource alt_music;
        private AudioSource dark;
        private AudioSource drop;
        private AudioSource dark_ambient;
        private AudioSource sound;
        private AudioSource happy;
        private AudioSource outAmbient;
        private AudioSource outDark;
        private AudioSource rain;
        private AudioSource thunder;
        [SerializeField]private AudioClip[] thunderSounds;
        private Animator Door;
        private FirstPersonController player;
        private bool enable = false;
        private bool switched = false;
        private Transform firstTarget;
        private Transform secondTarget;
        private Vector3 firstPos;
        private Vector3 secondPos;
        public int state = 0;
        private bool prev = false;
        private GameObject cube;
        public int loopNum = 0;
        bool soundPlay = false;
        private bool darkTrigger = false;
        private float lastThunder;
        private bool musicFade = false;
        public void MusicFade()
        {
            darkFigure.MusicFade();
            musicFade = true;
        }
        public void MusicTrigger()
        {
            darkTrigger = true;
            dark.Play();
        }
        public void SoundTrigger()
        {
            drop.Play();
        }
        public void Move()
        {
            if (!prev)
            {
                player.Disable();
                state = 1;
                prev = true;
            }
        }
        public void Trigger()
        {
        }
        public void ResetPrev()
        {
            prev = false;
            loopNum++;
            if (loopNum % 2 == 0)
            {
                secondPos = firstTarget.transform.position;
                firstPos = secondTarget.transform.position;
            }
            else
            {
                firstPos = firstTarget.transform.position;
                secondPos = secondTarget.transform.position;
            }
        }
        private void Start()
        {
            lastThunder = Time.time;
            TV = GameObject.Find("TVMain");
            AudioSource[] sounds;
            sounds = GameObject.Find("FirstPersonCharacter").GetComponents<AudioSource>();
            sound = sounds[0];
            music = sounds[1];
            alt_music = sounds[2];
            dark = sounds[3];
            dark_ambient = sounds[4];
            happy = sounds[5];
            outAmbient = sounds[7];
            outDark = sounds[6];
            rain = sounds[8];
            thunder = sounds[9];
            drop = sounds[10];
            RenderSettings.skybox = Skyboxes[0];
            triggerCube = GameObject.Find("TriggerCube");
            Door = GameObject.Find("Hallway_Door_Loop2").GetComponent<Animator>();
            cube = GameObject.Find("Cube Encasing");
            firstTarget = GameObject.Find("firstTarget").GetComponent<Transform>();
            secondTarget = GameObject.Find("secondTarget").GetComponent<Transform>();
            player = GameObject.Find("Player").GetComponent<FirstPersonController>();
            fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, 1.0f);
            player.Disable();
            firstPos = firstTarget.transform.position;
            secondPos = secondTarget.transform.position;
        }
        private void Update()
        {
            if (musicFade)
            {
                rain.volume = Mathf.Lerp(rain.volume, 0.0f, 1.0f * Time.deltaTime);
                sound.volume = Mathf.Lerp(sound.volume, 0.0f, 1.0f * Time.deltaTime);
                music.volume = Mathf.Lerp(music.volume, 0.0f, 1.0f * Time.deltaTime);
                alt_music.volume = Mathf.Lerp(alt_music.volume, 0.0f, 1.0f * Time.deltaTime);
                thunder.volume = Mathf.Lerp(thunder.volume, 0.0f, 1.0f * Time.deltaTime);
                happy.volume = Mathf.Lerp(happy.volume, 0.0f, 1.0f * Time.deltaTime);
                outDark.volume = Mathf.Lerp(outDark.volume, 0.0f, 1.0f * Time.deltaTime);
                outAmbient.volume = Mathf.Lerp(outAmbient.volume, 0.0f, 1.0f * Time.deltaTime);
                dark.volume = Mathf.Lerp(dark.volume, 0.0f, 1.0f * Time.deltaTime);
                dark_ambient.volume = Mathf.Lerp(dark_ambient.volume, 0.0f, 1.0f * Time.deltaTime);
                drop.volume = Mathf.Lerp(drop.volume, 0.0f, 1.0f * Time.deltaTime);
            }
            else
            {
                if (darkTrigger)
                dark.volume = Mathf.Lerp(dark.volume, 1.0f, 1.0f * Time.deltaTime);
            if (loopNum == 3)
            {
                if (Time.time - lastThunder > 20.0f)
                {
                    int n = Random.Range(1, thunderSounds.Length);
                    thunder.clip = thunderSounds[n];
                    thunder.PlayOneShot(thunder.clip);
                    // move picked sound to index 0 so it's not picked next time
                    thunderSounds[n] = thunderSounds[0];
                    thunderSounds[0] = thunder.clip;
                    lastThunder = Time.time;
                }
                TV.tag = "Tv";
                music.volume = Mathf.Lerp(music.volume, 0.0f, 1.0f * Time.deltaTime);
                outAmbient.volume = Mathf.Lerp(music.volume, 0.0f, 1.0f * Time.deltaTime);
                dark_ambient.volume = Mathf.Lerp(dark_ambient.volume, 1.0f, 1.0f * Time.deltaTime);
                rain.volume = Mathf.Lerp(rain.volume, 1.0f, 1.0f * Time.deltaTime);
                outDark.volume = Mathf.Lerp(outDark.volume, 1.0f, 1.0f * Time.deltaTime);
                if (!darkAmbientPlayed)
                {
                    outDark.Play();
                    rain.Play();
                    dark_ambient.Play();
                    darkAmbientPlayed = true;
                }

            }
            if (music.isPlaying)
            {
                alt_music.volume = Mathf.Lerp(alt_music.volume, 0.0f, 1.0f * Time.deltaTime);
                happy.volume = Mathf.Lerp(happy.volume, 0.0f, 1.0f * Time.deltaTime);
            }
            if (state == 1)
            {
                RenderSettings.skybox = Skyboxes[loopNum - 1];
                soundPlay = true;
                player.transform.position = Vector3.Lerp(player.transform.position, firstPos, 0.35f * Time.deltaTime);
                var lookPos = secondPos - player.transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);

                player.transform.rotation = Quaternion.Slerp(player.transform.rotation, rotation, Time.deltaTime * 3.0f);
            }
            else if (state == 2)
            {
                sound.volume = Mathf.Lerp(sound.volume, 0.0f, 1.0f * Time.deltaTime);
                if (!alt_music.isPlaying && loopNum == 1)
                {
                    alt_music.Play();
                    happy.Play();
                    outAmbient.Play();
                }
                if (!music.isPlaying && loopNum == 2)
                    music.Play();
                player.transform.position = Vector3.Lerp(player.transform.position, secondPos, 0.4f * Time.deltaTime);
                if (soundPlay)
                {
                    GetComponent<AudioSource>().Play();
                    soundPlay = false;
                }
                if (loopNum % 2 == 0)
                    Door.Play("Open");
                else
                    Door.Play("Open_Reverse");


            }
            if (state == 1 && Vector3.Distance(player.transform.position, firstPos) < 0.5)
            {
                state = 2;
            }

            if (Vector3.Distance(player.transform.position, secondPos) < 1)
            {
                cube.SetActive(false);
                
                player.Enable();
                state = 0;
            }
            if (CrossPlatformInputManager.GetButtonDown("Interact"))
                enable = true;
                if (enable)
                {
                    fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, Mathf.Lerp(fade.color.a, 0.0f, 0.1f));
                    text.color = new Color(text.color.r, text.color.g, text.color.b, Mathf.Lerp(text.color.a, 0.0f, 0.1f));
                    if (text.color.a <= 0.1f && fade.color.a <= 0.1f && !switched)
                    {
                        player.Enable();
                        switched = true;
                    }

                }
            }
        }

    }
}
