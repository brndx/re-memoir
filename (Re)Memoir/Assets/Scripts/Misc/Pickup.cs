﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
public class Pickup : MonoBehaviour
{

    private bool carrying;
    private GameObject carriedObject;
    private float distance = 2;
    private float rayDistance = 3;
    private float smooth = 5;
    void Start()
    {
    }

    void FixedUpdate()
    {
        if (carrying)
        {
            carry(carriedObject);
            checkDrop();
        }
        else
        {
            pickup();
        }
    }

    void rotateObject()
    {
        carriedObject.transform.Rotate(5, 10, 15);
    }

    void carry(GameObject o)
    {
        o.transform.position = Vector3.Lerp(o.transform.position, transform.position + transform.forward * distance, Time.deltaTime * smooth);
    }



    void pickup()
    {
        RaycastHit hit;
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            // Check if object that can be picked up is present
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, rayDistance))
            {
                if (hit.collider.gameObject.tag == "Pickup")
                {
                    carrying = true;
                    carriedObject = hit.collider.gameObject;
                    carriedObject.GetComponent<Rigidbody>().isKinematic = true;

                }
            }
        }
    }

    void checkDrop()
    {
        if (!CrossPlatformInputManager.GetButton("Fire1"))
        {
            dropObject();
        }
    }

    void dropObject()
    {
        carrying = false;
        carriedObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        carriedObject = null;
    }
}

