﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    // public float flickerSpeed, flickerBrightness, lightBrightness;
    public float lightBrightness;
    public Renderer myRenderer;
    public Color defaultColor, switchedOffColor;
    public Light myLight;

    public bool switchedOn = true;

     void Start()
    {
        //myRenderer = GetComponent<Renderer>();
    }

    /*
     void FixedUpdate()
    {
        float randomPerlinBrightness = Mathf.PerlinNoise(Time.time*flickerSpeed, 0.5f) * flickerBrightness;
        if(switchedOn)
        {
            myRenderer.material.SetColor("_EmissionColor", defaultColor * randomPerlinBrightness);
            myLight.intensity= randomPerlinBrightness * lightBrightness;
        }
        else
        {
            myRenderer.material.SetColor("_EmissionColor", switchedOffColor);
            myLight.intensity = 0;
        }
    }    
    */

    void FixedUpdate()
    {
        if(switchedOn)
        {
            myRenderer.material.SetColor("_EmissionColor", defaultColor);
            myLight.intensity = lightBrightness;
        }
        else
        {
            myRenderer.material.SetColor("_EmissionColor", switchedOffColor);
            myLight.intensity = 0;
        }
    }
}
