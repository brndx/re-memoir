using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private float m_GrabSensitivity = 1.0f;
        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private Text text;
        [SerializeField] private AudioClip[] m_WoodFootstepSounds;
        [SerializeField] private AudioClip[] m_TileFootstepSounds;
        [SerializeField] private AudioClip[] m_CarpetFootstepSounds;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        private Camera m_Camera;
        private bool m_Jump;
        private bool m_Crouch = false;
        private float m_CrouchSpeed = 3.5f;
        private float m_rayDistance = 3;
        private float m_StandPos = 1.6f;
        private float m_CrouchPos = 0.6f;
        private float m_YRotatiron;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        public bool m_usingController = false;
        private GameObject m_GrabTarget;
        private AudioSource m_AudioSource;
        private bool m_Grab = false;
        private bool enabled = true;
        private float lastStep = 0.0f;
        private RawImage crosshair;
        private RawImage hand;
        public bool itemLook = false;
        [SerializeField]private Texture[] icons;
        // Use this for initialization
        private void Start()
        {
           
            hand = GameObject.Find("Hand").GetComponent<RawImage>();
            crosshair = GameObject.Find("Crosshair").GetComponent<RawImage>();
            crosshair.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 1.0f);
            hand.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 0.0f);
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
			
        }

        // Update is called once per frame
        private void Update()
        {
            Cursor.visible = false;

            if (enabled)
            {

                if (!m_Grab)
                {
                    RotateView();
                    Cursor.lockState = CursorLockMode.Locked;
                }
                else
                {
                    Cursor.lockState = CursorLockMode.None;
                    if (!itemLook)
                    {
                        crosshair.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 1.0f);
                        hand.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 0.0f);
                    }
                }
                InteractCheck();
                Crouch();
                if (Input.GetKey(KeyCode.Joystick1Button0) || Input.GetKey(KeyCode.Joystick1Button2) || Input.GetKey(KeyCode.Joystick1Button4) || Input.GetKey(KeyCode.Joystick1Button8))
                    m_usingController = true;
                if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.C))
                    m_usingController = false;
                // the jump state needs to read here to make sure it is not missed
                if (!m_Jump)
                {
                    m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
                }

                if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
                {
                    StartCoroutine(m_JumpBob.DoBobCycle());
                    PlayLandingSound();
                    m_MoveDir.y = 0f;
                    m_Jumping = false;
                }
                if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
                {
                    m_MoveDir.y = 0f;
                }

                m_PreviouslyGrounded = m_CharacterController.isGrounded;
            }
        }
        private void Crouch()
        { 
            if (CrossPlatformInputManager.GetButtonDown("Crouch"))
            {
                if (m_Crouch)
                    m_Crouch = false;
                else
                    m_Crouch = true;
            }
            // Adjust camera height depending on crouch condition
            if (m_Crouch)
            {
                m_Camera.transform.position = Vector3.Lerp(m_Camera.transform.position, new Vector3(m_Camera.transform.position.x, m_CrouchPos, m_Camera.transform.position.z), Time.deltaTime * m_CrouchSpeed);
            }
            else
            {
                m_Camera.transform.position = Vector3.Lerp(m_Camera.transform.position, new Vector3(m_Camera.transform.position.x, m_StandPos, m_Camera.transform.position.z), Time.deltaTime * m_CrouchSpeed);
            }
        }
        public void ChangeFootstepSound(int mode)
        {
            if (mode == 1)
                m_FootstepSounds = m_WoodFootstepSounds;
            else if (mode == 2)
                m_FootstepSounds = m_TileFootstepSounds;
            else if (mode == 3)
                m_FootstepSounds = m_CarpetFootstepSounds;

        }
        private void InteractCheck()
        {
            RaycastHit hit;
            // Check if any interactive objects are present
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, m_rayDistance))
            {

                if (hit.transform.gameObject.tag == "Interactive" || hit.transform.gameObject.tag == "Transition")
                {
                    if (hit.transform.gameObject.tag == "Interactive")
                        hand.texture = icons[1];
                    else
                        hand.texture = icons[0];
                        // Activate HUD info text
                        if (!m_Grab)
                    {
                        hit.transform.gameObject.SendMessage("Info");
                    }
                    // Call the interact method of the intersected object
                    if (CrossPlatformInputManager.GetButtonDown("Interact"))
                    {
                        if (hit.transform.gameObject.tag == "Interactive")
                        {
                            m_GrabTarget = hit.transform.gameObject;
                            if (m_Grab)
                                m_Grab = false;
                            else
                                m_Grab = true;
                        }
                        else
                            hit.transform.gameObject.SendMessage("Interact");
                    }
                }
                else
                {
                    hand.texture = icons[0];
                    if (!itemLook)
                    {
                        crosshair.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 1.0f);
                        hand.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 0.0f);
                    }
                }
            }
            else
            {
                if (!itemLook)
                {
                    crosshair.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 1.0f);
                    hand.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 0.0f);
                }
            }
        }
        private void PlayLandingSound()
        {
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
            m_NextStep = m_StepCycle + .5f;
        }


        private void FixedUpdate()
        {
            if (!enabled)
            {
                if (Time.time - lastStep > 0.482f)
                {
                    PlayFootStepAudio();
                    lastStep = Time.time;
                }
            }
            if (enabled)
            {
                if (m_Grab)
            {
                if (m_GrabTarget != null)
                {
                    m_GrabTarget.SendMessage("Interact", m_GrabSensitivity);
                }
                if (!CrossPlatformInputManager.GetButton("Interact"))
                {
                    m_Grab = false;
                    m_GrabTarget.SendMessage("Detach");
                    m_GrabTarget = null;
                }
            }
            float speed;
            GetInput(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward*m_Input.y + transform.right*m_Input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = desiredMove.x*speed;
            m_MoveDir.z = desiredMove.z*speed;


            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

                if (m_Jump)
                {
                    m_MoveDir.y = m_JumpSpeed;
                    PlayJumpSound();
                    m_Jump = false;
                    m_Jumping = true;
                }
            }
            else
            {
                m_MoveDir += Physics.gravity*m_GravityMultiplier*Time.fixedDeltaTime;
            }
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);
           
                ProgressStepCycle(speed);
                UpdateCameraPosition(speed);
                if (!m_Grab)
                    m_MouseLook.UpdateCursorLock();
            }
        }
        
            public void DrawIcon()
        {
            // HUD Info Text
            crosshair.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 0.0f);
            hand.color = new Vector4(crosshair.color.r, crosshair.color.b, crosshair.color.g, 1.0f);

        }
        private void PlayJumpSound()
        {
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }

        public void Enable()
        {
            enabled = true;
        }
        public void Disable()
        {
            enabled = false;
        }
        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            if (!m_UseHeadBob)
            {
                return;
            }
            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                      (speed*(m_IsWalking ? 1f : m_RunstepLenghten)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
            m_Camera.transform.localPosition = newCameraPosition;
        }


        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxisRaw("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxisRaw("Vertical");

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            if (!Input.GetKey(KeyCode.LeftShift) && !CrossPlatformInputManager.GetButton("Sprint"))
                m_IsWalking = true;
            else
                m_IsWalking = false;
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }


        private void RotateView()
        {
            m_MouseLook.Init(transform, m_Camera.transform);
            m_MouseLook.LookRotation (transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
    }
}
