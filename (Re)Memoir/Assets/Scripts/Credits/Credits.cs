﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Credits : MonoBehaviour {
    private AudioSource music;
    private bool fadeStart = false;
	void Start () {
        music = GetComponent<AudioSource>();
	}

    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Credits") && !fadeStart)
        {
            StartCoroutine(MusicFade());
            fadeStart = true;
        }
        if (fadeStart)
        {
            music.volume = Mathf.Lerp(music.volume, 0.0f, 1.0f * Time.deltaTime);
        }
    }
    private IEnumerator MusicFade ()
    {
        yield return new WaitForSeconds(7.0f);
        SceneManager.LoadScene("Main Menu");
    }
}
