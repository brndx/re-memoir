﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Transit_Door;
namespace Transitions
{
    public class InteractiveTransition : MonoBehaviour
    {
        private GameObject Loop;
        private GameObject Loop2;
        private GameObject Loop3;
        private Door door;
        
        public void ActivateOne()
        {
            door.Enable();
            Loop2.SetActive(true);
        }
        public void ActivateTwo()
        {
            door.Enable();
            Loop.SetActive(false);
            Loop3.SetActive(true);
        }
        void Start()
        {
            door = GameObject.Find("Hallway_Door_Loop2").GetComponent<Door>();

            Loop = GameObject.Find("Loop1");
            Loop2 = GameObject.Find("Loop2");
            Loop3 = GameObject.Find("Loop3");
            Loop2.SetActive(false);
            Loop3.SetActive(false);


        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
