﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
    public class PortalTeleporter : MonoBehaviour
    {
        private FirstPersonController player;
        [SerializeField] private Transform reciever;
        [SerializeField] private GameObject camera;
        private bool playerIsOverLapping = false;
    private float offSet = 180.0f;
        private void Start()
        {
            player = GameObject.Find("Player").GetComponent<FirstPersonController>();
        }
        void Update()
        {
            if (Vector3.Distance(player.transform.position, transform.position) < 10)
                camera.SetActive(true);
            else
                camera.SetActive(false);
            // Collision trigger check
            if (playerIsOverLapping)
            {
                Vector3 portalToPlayer = player.GetComponent<Transform>().position - transform.position;
                float dotProduct = Vector3.Dot(transform.up, portalToPlayer);
            // Send player to portal exit
                if (dotProduct < 0f)
                {
                    float rotationDiff = -Quaternion.Angle(transform.rotation, reciever.rotation);
                    rotationDiff += offSet;
                    player.GetComponent<Transform>().Rotate(Vector3.up, rotationDiff);
                    Vector3 positionOffest = Quaternion.Euler(0f, rotationDiff, 0f) * portalToPlayer;
                    player.GetComponent<Transform>().position = reciever.position + positionOffest;
                    playerIsOverLapping = false;
                }
            }

        }

        void OnTriggerEnter(Collider other)
        {
            if (other == player.GetComponent<Collider>())
                playerIsOverLapping = true;
        }

        void OnTriggerExit(Collider other)
        {
            if (other == player.GetComponent<Collider>())
                playerIsOverLapping = false;
        }
    }
