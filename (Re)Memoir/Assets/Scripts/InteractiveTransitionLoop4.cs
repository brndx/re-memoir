﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Transit_Door;
namespace Transitions
{
    public class InteractiveTransitionLoop4 : MonoBehaviour
    {
        private GameObject Loop;
        private GameObject Loop2;
        private GameObject Loop3;
        private GameObject Loop4;
        private GameObject Loop5;
        private GameObject Loop6;
        private Door door;

        public void ActivateOne()
        {
            door.Enable();

        }
        public void ActivateTwo()
        {
            door.Enable();

        }
        void Start()
        {
            door = GameObject.Find("Hallway_Door_Loop2").GetComponent<Door>();

            Loop = GameObject.Find("Loop1");
            Loop2 = GameObject.Find("Loop2");
            Loop3 = GameObject.Find("Loop3");
            Loop4 = GameObject.Find("Loop4");
            Loop5 = GameObject.Find("Loop5");
            Loop6 = GameObject.Find("Loop6");
            Loop3.SetActive(false);
            Loop4.SetActive(false);
            Loop5.SetActive(false);
            Loop6.SetActive(false);


        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
