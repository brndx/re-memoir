﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EmissionControl : MonoBehaviour
{
    [SerializeField] private Material material; //ref to the emissive material
    [SerializeField] private float timeOn = 0.1f;
    [SerializeField] private float timeOff = 0.5f;
    private float changeTime = 0f;


	void Awake()
    {
        //emission doesnt work on build with .EnableKeyword
        material.DisableKeyword("_EMISSION");
        
	}

    void Update ()
    {
        //need to flicker on and off between a couple of seconds 
        //turn on after x time when game starts

        if(Time.time>changeTime) // testing if emission works. this can be changed
        {
            material.EnableKeyword("_EMISSION");
        }
		
	}
}
