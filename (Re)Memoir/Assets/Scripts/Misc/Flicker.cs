﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{
    public float flickerSpeed = 0.1f, flickerBrightness = 3f, lightBrightness = 0.5f;
    private Renderer myRenderer;
    public Color defaultColor, switchedOffColor;
    public Light myLight;

    [Tooltip("Toggle this to stop the flickering")]
    public bool switchedOn = true;
    // change this from a light-switch to toggle the flickering


    void Start ()
    {
        myRenderer = GetComponent<Renderer>();
    }
	

	void FixedUpdate ()
    {
        float randomPerlinBrightness = Mathf.PerlinNoise(Time.time * flickerSpeed, 0.5f) * flickerBrightness;
        if (switchedOn)
        {
            // switched on so set the random brightness
            myRenderer.material.SetColor("_EmissionColor", defaultColor * randomPerlinBrightness);
            myLight.intensity = randomPerlinBrightness * lightBrightness;
        } else {
            // it's turned off - set the color to a darker version and the light is off
            myRenderer.material.SetColor("_EmissionColor", switchedOffColor);
            myLight.intensity = 0;
        }
    }


}
