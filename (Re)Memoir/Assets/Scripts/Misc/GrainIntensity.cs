﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

public class GrainIntensity : MonoBehaviour
{
    public PostProcessingProfile cameraProfile;
   

    void Start()
    {
        cameraProfile.grain.enabled = false;
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            cameraProfile.grain.enabled = true;
            StartCoroutine(EndGameCredits());
            
        }
       
    }

    IEnumerator EndGameCredits()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(2);
        //make end game credits for LoadScene(2) 
    }



}
